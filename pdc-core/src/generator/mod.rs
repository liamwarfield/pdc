use crate::scenario::Scenario;
use crossbeam_channel::Receiver;

pub mod fuzzy_request_generator;
pub mod hyper;
pub mod reqwest_generator;

/// Traffic Generators are the bread and butter of pdc.
/// Structs that implement TrafficGenerator will take in
/// a scenario and try to replicate the network traffic
/// described by that scenario.
pub trait TrafficGenerator<T: Scenario>: Send + Sync {
    /// Give a bounded channel that sends time series data on the send rate of
    /// a generator. The data is encoded as
    /// `(Miliseconds since start of scenario, counts/sec)`.
    fn get_data_rate_channel(&self) -> Receiver<(f32, f32)>;
    /// Give a bounded channel that sends the total amount of packets/requests sent
    fn get_sent_packets_channel(&self) -> Receiver<usize>;
    /// Give a bounded channel that sends time series data on the send rate of
    /// a generator. The data is encoded as
    /// `(Miliseconds since start of scenario, counts/sec)`.
    fn get_error_rate_channel(&self) -> Receiver<(f32, f32)>;
    /// Send a single packet per call, used for debuging and low traffic testing.
    fn send_packet(&mut self);
    /// Sets a [`Scenario`] for the generator to try and replicate.
    fn set_scenario(&mut self, schem: T);
    /// Gets the current [`Scenario`]
    fn get_scenario(&self) -> &T;
    /// Runs the current scenario
    fn run_scenario(&mut self);
    /// Run the generator at full throttle!
    fn fire_hose(&mut self);
}

#[derive(Clone, Debug)]
pub struct HttpConfig {
    pub url: String,
    /// HTTP Method, Defaults to GET.
    pub method: String,
    pub body: String,
}
