use super::Scenario;

/// A scenario that represents an exponential increase in traffic. This represents
/// A*e^(Bx) + C, where x is the time in milisecond
#[allow(non_snake_case)]
#[derive(Clone)]
pub struct ExponentialScenario {
    pub A: f32,
    pub B: f32,
    pub C: f32,
    pub duration: u128,
}

impl Scenario for ExponentialScenario {
    fn rate(&self, time_elasped: std::time::Duration) -> f32 {
        self.A * (self.B * time_elasped.as_millis() as f32).exp() + self.C
    }

    fn duration(&self) -> u128 {
        self.duration
    }

    fn max_rate(&self) -> f32 {
        self.A * (self.B * self.duration as f32).exp() + self.C
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::time::Duration;

    #[test]
    fn exp_scenario() {
        //= 1000 * EXP(A3/3000) + 20000
        let scene = ExponentialScenario {
            A: 1000.,
            B: 1. / 3000.,
            C: 20000.,
            duration: 10000,
        };
        assert_eq!(scene.duration(), 10000);
        assert!((scene.rate(Duration::from_millis(0)) - 21000.).abs() < 0.01);
        assert!((scene.rate(Duration::from_millis(500)) - 21181.36041).abs() < 0.01);
        assert!((scene.rate(Duration::from_millis(10000)) - 48031.62489).abs() < 0.01);
        assert!((scene.max_rate() - 48031.62489).abs() < 0.01);
    }
}
