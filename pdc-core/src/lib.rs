//! # pdc-core
//!
//! pdc-core is library that gives access to high proformance load testing primatives.
//!
//! `pdc-core` exposes most of its functionality via the `TrafficGenerator` and `Scenario` traits.
//! A `TrafficGenerator` does what it says on the tin, it generates some type of network traffic.
//! `TrafficGenerator`'s have two different modes: track a load curve, or max traffic (firehose).
//! Generators don't necessarily have to send HTTP traffic either.
//!
//! A `Scenario`'s job is fairly simple. Given an elapsed time T (ms), a `Scenario` should produce a target
//! rate R (requests/sec).
//!
//! `pdc-core` provides functionality out of the box via handy implementations like
//! [`ReqwestGenerator`](struct@crate::generator::reqwest_generator::ReqwestGenerator) and [`ConstantScenario`](struct@crate::scenario::ConstantScenario)
//! ```
//! use pdc_core::generator::reqwest_generator::ReqwestGenerator;
//! use pdc_core::generator::TrafficGenerator;
//! use pdc_core::scenario::ConstantScenario;
//! use reqwest;
//! use std::str::FromStr;
//! use std::thread;
//!
//!
//! // generate 10000 packets per second for 100000 ms (100 sec)
//! let scene = ConstantScenario {
//!     rate: 10000.,
//!     duration: 100000,
//! };
//!
//! let max_tcp_connections = 300;
//! let worker_count = 6;
//! let warmup = true;
//! let method = reqwest::Method::from_str("GET").unwrap();
//! let req = reqwest::Request::new(
//!     method,
//!     reqwest::Url::from_str("http://127.0.0.1:8080").unwrap(),
//! );
//!
//! // Create a generator backed by reqwest
//! let mut generator = ReqwestGenerator::new(scene, max_tcp_connections, worker_count, warmup, req);
//!
//! // Create a Reciever to get data while the generator is running
//! let num_sent = generator.get_sent_packets_channel();
//!
//! thread::spawn(move || {
//!     generator.run_scenario();
//! });
//!
//! // Continuesly print out the number of packets sent.
//! loop {
//!     if let Ok(n) = num_sent.try_recv() {
//!         println!("{}", n);
//!     } else {
//!         thread::sleep(std::time::Duration::from_millis(100));
//!     }
//! }
//!```

pub mod generator;
pub mod scenario;
