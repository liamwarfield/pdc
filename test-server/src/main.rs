use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Request, Response, Server};
use std::convert::Infallible;
use std::net::SocketAddr;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;

#[tokio::main]
async fn main() {
    // Construct our SocketAddr to listen on...
    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));

    let counter = Arc::new(AtomicUsize::new(0));

    // And a MakeService to handle each connection...
    let make_service = make_service_fn(move |_conn| {
        let counter = counter.clone();
        async move {
            Ok::<_, Infallible>(service_fn(move |_req: Request<Body>| {
                let counter = counter.clone();
                async move { Ok::<_, Infallible>(use_counter(counter)) }
            }))
        }
    });

    // Then bind and serve...
    let server = Server::bind(&addr).serve(make_service);

    // And run forever...
    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}

fn use_counter(counter: Arc<AtomicUsize>) -> Response<Body> {
    let data = counter.fetch_add(1, Ordering::Relaxed);
    if data % 10 == 0 {
        Response::builder()
            .status(505)
            .body(Body::from("OMG a fake error!"))
            .unwrap()
    } else {
        Response::new(Body::from(format!("Counter: {}\n", data)))
    }
}
