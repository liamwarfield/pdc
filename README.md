# pdc
`pdc` is a packet generator written in rust for load testing networking protocols.
`pdc-core` provides a set of extensible load-testing primitives. The bread and butter
of `pdc-core` are the `TrafficGenerator` trait and the `Scenario` trait. A `TrafficGenertor`,
takes in a load curve specified by a `Scenario` and will attempt to emulate that load.

![pdc cli demo](./demo.gif)

# How to use pdc
`pdc` can be used as either a library by importing `pdc_core` into your projects, or as a
free standing cli.

## Using the pdc CLI
The Cli takes in a yaml that defines a loadtest and then runs it `pdc run <test.yaml>`. The yaml
defines how a test is run:
```yaml
---
# Define the load curve that you want PDC to emulate
Scenario:
  - CSV: "./pdc-core/test.csv"
  - Linear:
      starting_rate: 20000.0 
      slope: 2000.0
      duration: 10000
  - Linear:
      starting_rate: 40000.0
      slope: -4000.0
      duration: 7000
  - Constant:
      rate: 12000.0
      duration: 2000
Generator:
  # Use the hyper based generator
  HyperGenerator:
    # The number of allowed TCP connections.
    max_open_connections: 500
    threads: 11
    warmup: true
    url: "http://127.0.0.1:3000"
    method: GET
    body: ""
```

## Supported Loads
`pdc` supports linearly increasing, exponentially increasing, constant, and custom (csv defined) loads.

## Installing the pdc CLI
For now, `cargo install --path pdc-cli`.

## pdc Lib
`pdc-core` exposes most of its functionality via the `TrafficGenerator` and `Scenario` traits.
A `TrafficGenerator` does what it says on the tin, it generates some type of network traffic.
`TrafficGenerator`'s have two different modes: track a load curve, or max traffic (firehose).
Generators don't necessarily have to send HTTP traffic either, in the future lower level traffic
like ICMP will be supported. 

A `Scenario`'s job is fairly simple. Given an elapsed time T (ms), a `Scenario` should produce a target
rate R (requests/sec).

`pdc-core` provides functionality out of the box via handy implementations like `ReqwestGenerator`.

# What are Use Cases for pdc?
`pdc` is great for doing local/LAN load testing of a web service. As more `TrafficGenerator`s are
implimented, more use cases should become available.

# Current limitations
Currently `pdc` only supports simple `reqwest` and `hyper` based generators.
These generators wait for the full round trip time of a request (or a timeout) before reusing a TCP connection.
This means that the maximum requests per second are severely limited by the latency of a connection.

The `hyper` based generator is more proformant because it uses HTTP multplexing, but requires that the
target server support HTTP2.

# Future plans

1. PDC is great for testing locally, but throughput becomes an issue once latency is introduced. A generator
that is less affected by latency is on the docket.

2. Support a flow of requests/packets. AKA "send a `GET localhost:8080/`, then POST "foo" to `localhost:8080/bar`".

3. Lower level networking protocol generators. I (Liam Warfield) work on IGMP/MLD, and
would like access to L3 and L2 Load testing tooling.

4. Interactive scenario builder. Leverage `tui_rs` and `crossterm` to make an interactive
scenario builder.

5. Use lowwer level libraries to send traffic (like h2).
