use reqwest;
use std::error::Error;
use std::time::Duration;

/// This will use a generator config later.
pub fn pew(url: String) -> Result<(), Box<dyn Error>> {
    loop {
        let body = reqwest::blocking::get(url.clone())?.text()?;

        println!("{:?}", body);
        std::thread::sleep(Duration::from_millis(1000));
    }
}
