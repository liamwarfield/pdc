use pdc_core::generator::hyper::HyperGenerator;
use pdc_core::generator::{
    fuzzy_request_generator::FuzzyReqwestGenerator, reqwest_generator::ReqwestGenerator,
    HttpConfig, TrafficGenerator,
};
use pdc_core::scenario::{
    composite::CompositeScenario, custom::CustomScenario, exponential::ExponentialScenario,
    ConstantScenario, LinearScenario, Scenario,
};
use reqwest::Url;
use serde::{Deserialize, Serialize};
use std::str::FromStr;

#[derive(Serialize, Deserialize)]
pub struct PDConfig {
    #[serde(rename = "Scenario")]
    scenario: Vec<ScenarioConfig>,
    #[serde(rename = "Generator")]
    generator: GeneratorConfig,
}

#[derive(Serialize, Deserialize)]
pub enum ScenarioConfig {
    /// Path to a CSV to construct a custom scenario
    CSV(std::path::PathBuf),
    /// Starting Rate, Slope
    Linear {
        starting_rate: f32,
        slope: f32,
        duration: u128,
    },
    Constant {
        rate: f32,
        duration: u128,
    },
    Exponential(f32, f32, f32, u128),
}

#[derive(Serialize, Deserialize)]
pub enum GeneratorConfig {
    Reqwest(ReqwestConfig),
    FuzzyReqwest(ReqwestConfig),
    HyperGenerator(ReqwestConfig),
}

#[derive(Serialize, Deserialize)]
pub struct ReqwestConfig {
    /// This is the maximum amount of TCP file descriptors **being used** at any one time.
    /// The actual number of open connections may be higher, because it takes
    /// time to purge old fd's.
    max_open_connections: usize,
    /// Specify the amount of threads used to send requests. Setting this to 0 will use all
    /// available.
    #[serde(default)]
    threads: usize,
    /// Let this generator warm up to the rate at t=0 before running the test.
    #[serde(default = "default_warmup")]
    warmup: bool,
    url: String,
    /// HTTP Method, Defaults to GET.
    #[serde(default = "default_http_method")]
    method: String,
    #[serde(default)]
    body: String,
}

impl PDConfig {
    pub fn new(generator: GeneratorConfig, scenario: Vec<ScenarioConfig>) -> Self {
        PDConfig {
            generator,
            scenario,
        }
    }
    /// Returns A generator based off of the config
    pub fn create_generator(self) -> Box<dyn TrafficGenerator<CompositeScenario>> {
        let mut scenarios: Vec<Box<dyn Scenario>> = Vec::new();
        for s_config in self.scenario {
            match s_config {
                ScenarioConfig::CSV(f) => scenarios.push(Box::new(CustomScenario::from_csv(f))),
                ScenarioConfig::Linear {
                    starting_rate,
                    slope,
                    duration,
                } => scenarios.push(Box::new(LinearScenario {
                    starting_rate,
                    slope,
                    duration,
                })),
                ScenarioConfig::Constant { rate, duration } => {
                    scenarios.push(Box::new(ConstantScenario { rate, duration }))
                }
                ScenarioConfig::Exponential(a, b, c, duration) => {
                    //TODO fix this horible naming
                    scenarios.push(Box::new(ExponentialScenario {
                        A: a,
                        B: b,
                        C: c,
                        duration,
                    }))
                }
            }
        }
        let scene = CompositeScenario::new(scenarios);
        match self.generator {
            GeneratorConfig::Reqwest(conf) => {
                let method = reqwest::Method::from_str(conf.method.as_str()).unwrap();
                let req = reqwest::Request::new(method, Url::from_str(conf.url.as_str()).unwrap());
                Box::new(ReqwestGenerator::new(
                    scene,
                    conf.max_open_connections,
                    conf.threads,
                    conf.warmup,
                    req,
                ))
            }
            GeneratorConfig::FuzzyReqwest(conf) => {
                let method = reqwest::Method::from_str(conf.method.as_str()).unwrap();
                let req = reqwest::Request::new(method, Url::from_str(conf.url.as_str()).unwrap());
                Box::new(FuzzyReqwestGenerator::new(
                    scene,
                    conf.max_open_connections,
                    conf.max_open_connections,
                    conf.warmup,
                    req,
                ))
            }
            GeneratorConfig::HyperGenerator(conf) => {
                let req = HttpConfig {
                    url: conf.url,
                    method: conf.method,
                    body: conf.body,
                };
                Box::new(HyperGenerator::new(
                    scene,
                    req,
                    conf.max_open_connections,
                    conf.threads,
                    conf.warmup,
                ))
            }
        }
    }
}

impl ReqwestConfig {
    pub fn new(
        max_open_connections: usize,
        threads: usize,
        warmup: bool,
        url: String,
        method: String,
        body: String,
    ) -> Self {
        ReqwestConfig {
            max_open_connections,
            threads,
            warmup,
            url,
            method,
            body,
        }
    }
}

fn default_http_method() -> String {
    "GET".to_string()
}

fn default_warmup() -> bool {
    true
}
