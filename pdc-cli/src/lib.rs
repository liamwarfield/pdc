use std::{path::PathBuf, time::Duration};

use csv::Writer;
use pdc_core::scenario::Scenario;
use serde::Serialize;

pub mod args;
pub mod config;
pub mod headless;
pub mod pew;
pub mod tui;

#[derive(Debug, Serialize)]
struct Record {
    #[serde(rename = "Time (ms)")]
    t1: Option<f64>,
    #[serde(rename = "Success Rate (requests/second)")]
    sr: Option<f64>,
    #[serde(rename = "Time (ms)")]
    t2: Option<f64>,
    #[serde(rename = "Error Rate (requests/second)")]
    er: Option<f64>,
    #[serde(rename = "Time (ms)")]
    t3: Option<f64>,
    #[serde(rename = "Target Rate (requests/second)")]
    tr: Option<f64>,
}

fn export_csv(
    succ_data: Vec<(f64, f64)>,
    error_data: Vec<(f64, f64)>,
    target_data: Vec<(f64, f64)>,
    total_packets: usize,
    path: &PathBuf,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut wtr = Writer::from_path(path)?;

    let mut s_iter = succ_data.into_iter();
    let mut e_iter = error_data.into_iter();
    let mut t_iter = target_data.into_iter();

    let mut t1;
    let mut sr;
    let mut t2;
    let mut er;
    let mut t3;
    let mut tr;
    loop {
        let mut num_writen = 0;
        if let Some(sd) = s_iter.next() {
            num_writen += 1;
            t1 = Some(sd.0);
            sr = Some(sd.1);
        } else {
            t1 = None;
            sr = None;
        }
        if let Some(sd) = e_iter.next() {
            num_writen += 1;
            t2 = Some(sd.0);
            er = Some(sd.1);
        } else {
            t2 = None;
            er = None;
        }
        if let Some(sd) = t_iter.next() {
            num_writen += 1;
            t3 = Some(sd.0);
            tr = Some(sd.1);
        } else {
            t3 = None;
            tr = None;
        }
        if num_writen == 0 {
            break;
        }
        wtr.serialize(Record {
            t1,
            sr,
            t2,
            er,
            t3,
            tr,
        })?;
    }
    wtr.write_record(&["Total Packets sent", "", "", "", "", ""])?;
    wtr.serialize((total_packets, "", "", "", "", ""))?;
    wtr.flush()?;
    Ok(())
}

fn create_target_dataset(scene: &dyn Scenario) -> Vec<(f64, f64)> {
    let dur = scene.duration() / 100;
    let mut ret = Vec::new();
    for i in 0..dur {
        let t = i * 100;
        ret.push((t as f64, scene.rate(Duration::from_millis(t as u64)) as f64))
    }
    ret
}
