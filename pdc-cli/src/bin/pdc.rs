use pdc_cli::args::{PdcArgs, PdcSubCommand};
use pdc_cli::config::PDConfig;
use pdc_cli::headless::run_headless;
use pdc_cli::pew::{self, pew};
use pdc_cli::tui::run_tui;
use structopt::StructOpt;

use std::error::Error;

use std::fs::File;
use std::io::BufReader;

fn main() -> Result<(), Box<dyn Error>> {
    let args: PdcArgs = PdcArgs::from_args();
    match args.subcommand {
        PdcSubCommand::Run {
            config,
            fire,
            headless,
            output_file,
        } => {
            let f = File::open(config)?;
            let rdr = BufReader::new(f);
            let conf: PDConfig = serde_yaml::from_reader(rdr)?;
            let generator = conf.create_generator();
            if headless {
                run_headless(generator, fire, output_file)
            } else {
                run_tui(generator, fire, output_file)
            }
        }
        PdcSubCommand::Pew { url } => pew(url),
        _ => {
            todo!()
        }
    }
}
