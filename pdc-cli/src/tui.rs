use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use pdc_core::{generator::TrafficGenerator, scenario::Scenario};
use std::{
    error::Error,
    io,
    path::PathBuf,
    thread,
    time::{Duration, Instant},
};
use tui::{
    backend::{Backend, CrosstermBackend},
    layout::{Alignment, Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    symbols,
    text::{Span, Spans},
    widgets::{Axis, Block, BorderType, Borders, Chart, Dataset, GraphType, Paragraph},
    Frame, Terminal,
};

use num_format::{Locale, ToFormattedString};

use crossbeam_channel::Receiver;

use crate::export_csv;

#[derive(PartialEq)]
enum InputMode {
    Normal,
    Editing,
    InputEntered,
    Quiting,
}

struct PDCApp {
    input_mode: InputMode,
    input: String,
    output_file: PathBuf,
    rate_data: Vec<(f64, f64)>,
    error_data: Vec<(f64, f64)>,
    target_data: Vec<(f64, f64)>,
    suc_rate_channel: Receiver<(f32, f32)>,
    error_rate_channel: Receiver<(f32, f32)>,
    total_packet_channel: Receiver<usize>,
    duration: u128,
    max_rate: f64,
    packets_sent: usize,
}

impl PDCApp {
    fn new(
        scene: &dyn Scenario,
        suc_rate_channel: Receiver<(f32, f32)>,
        error_rate_channel: Receiver<(f32, f32)>,
        total_packet_channel: Receiver<usize>,
        output_file: PathBuf,
    ) -> PDCApp {
        let rate_data = Vec::new();
        let error_data = Vec::new();
        let target_data = crate::create_target_dataset(scene);
        PDCApp {
            input_mode: InputMode::Normal,
            input: String::new(),
            output_file,
            rate_data,
            error_data,
            target_data,
            suc_rate_channel,
            error_rate_channel,
            total_packet_channel,
            duration: scene.duration(),
            max_rate: scene.max_rate() as f64,
            packets_sent: 0,
        }
    }

    fn update_data(&mut self) {
        for rate in self.suc_rate_channel.try_iter() {
            if rate.1 as f64 > self.max_rate {
                self.max_rate = rate.1 as f64 * 1.10;
            }
            self.rate_data.push((rate.0 as f64, rate.1 as f64));
        }

        for rate in self.error_rate_channel.try_iter() {
            self.error_data.push((rate.0 as f64, rate.1 as f64));
            if rate.1 as f64 > self.max_rate {
                self.max_rate = rate.1 as f64 * 1.10;
            }
        }

        if let Ok(np) = self.total_packet_channel.try_recv() {
            self.packets_sent = np;
        }
    }
}

pub fn run_tui<T: 'static + Scenario>(
    generator: Box<dyn TrafficGenerator<T>>,
    fire: bool,
    output_file: PathBuf,
) -> Result<(), Box<dyn Error>> {
    // setup terminal
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // create app and run it
    let tick_rate = Duration::from_millis(250);
    let res = run_app(&mut terminal, tick_rate, generator, fire, output_file);

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{:?}", err)
    }

    Ok(())
}

fn run_app<B: Backend, T: 'static + Scenario>(
    terminal: &mut Terminal<B>,
    tick_rate: Duration,
    mut generator: Box<dyn TrafficGenerator<T>>,
    fire: bool,
    output_file: PathBuf,
) -> io::Result<()> {
    let last_tick = Instant::now();

    let scene = generator.get_scenario();
    let suc_rate_channel = generator.get_data_rate_channel();
    let error_channel = generator.get_error_rate_channel();
    let packet_channel = generator.get_sent_packets_channel();
    let mut app = PDCApp::new(
        scene,
        suc_rate_channel,
        error_channel,
        packet_channel,
        output_file,
    );

    thread::spawn(move || {
        if fire {
            generator.fire_hose();
        } else {
            generator.run_scenario();
        }
    });

    let start = Instant::now();
    let mut test_over = false;

    loop {
        if app.input_mode == InputMode::Quiting {
            return Ok(());
        }
        terminal.draw(|f| ui(f, &mut app, test_over))?;

        thread::sleep(Duration::from_millis(10));

        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));
        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                match app.input_mode {
                    InputMode::Normal => {
                        if let KeyCode::Char('q') = key.code {
                            app.input_mode = InputMode::Quiting;
                        }
                    }
                    InputMode::Editing => match key.code {
                        KeyCode::Enter => app.input_mode = InputMode::InputEntered,
                        KeyCode::Char(c) => {
                            app.input.push(c);
                        }
                        KeyCode::Backspace => {
                            app.input.pop();
                        }
                        KeyCode::Esc => {
                            app.input_mode = InputMode::Normal;
                        }
                        _ => {}
                    },
                    _ => {}
                }
            }
        }
        if start.elapsed().as_millis() > app.duration && !test_over {
            test_over = true;
            app.input_mode = InputMode::Editing;
        }
    }
}

fn ui<B: Backend>(f: &mut Frame<B>, app: &mut PDCApp, test_over: bool) {
    app.update_data();

    let size = f.size();
    let window = [0., app.duration as f64];
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Ratio(3, 4), Constraint::Ratio(1, 4)].as_ref())
        .split(size);
    let x_labels = vec![
        Span::styled(
            format!("{}", window[0]),
            Style::default().add_modifier(Modifier::BOLD),
        ),
        Span::raw(format!("{}", (window[0] + window[1]) / 2.0)),
        Span::styled(
            format!("{}", window[1]),
            Style::default().add_modifier(Modifier::BOLD),
        ),
    ];

    let datasets = vec![
        Dataset::default()
            .name("Target Packet Rate")
            .marker(symbols::Marker::Braille)
            .graph_type(GraphType::Line)
            .style(Style::default().fg(Color::Cyan))
            .data(app.target_data.as_slice()),
        Dataset::default()
            .name("Packet Rate")
            .marker(symbols::Marker::Braille)
            .graph_type(GraphType::Line)
            .style(Style::default().fg(Color::Green))
            .data(app.rate_data.as_slice()),
        Dataset::default()
            .name("Error Rate")
            .marker(symbols::Marker::Braille)
            .graph_type(GraphType::Line)
            .style(Style::default().fg(Color::Red))
            .data(app.error_data.as_slice()),
    ];

    let chart = Chart::new(datasets)
        .block(
            Block::default()
                .title(Span::styled(
                    "Packet Rate",
                    Style::default()
                        .fg(Color::Cyan)
                        .add_modifier(Modifier::BOLD),
                ))
                .title_alignment(Alignment::Center)
                .border_type(BorderType::Rounded)
                .borders(Borders::ALL),
        )
        .x_axis(
            Axis::default()
                .title("Time (ms)")
                .style(Style::default().fg(Color::Gray))
                .labels(x_labels)
                .bounds(window),
        )
        .y_axis(
            Axis::default()
                .title("Packets Per Second")
                .style(Style::default().fg(Color::Gray))
                .labels(vec![
                    Span::styled("0", Style::default().add_modifier(Modifier::BOLD)),
                    Span::raw(format!("{}", app.max_rate / 1.6666)),
                    Span::styled(
                        format!("{}", app.max_rate * 1.20),
                        Style::default().add_modifier(Modifier::BOLD),
                    ),
                ])
                .bounds([0., app.max_rate * 1.20]),
        );
    f.render_widget(chart, chunks[0]);

    let mut text = vec![
        Spans::from(Span::styled(
            format!(
                "Total Packets Sent: {}",
                app.packets_sent.to_formatted_string(&Locale::en)
            ),
            Style::default().add_modifier(Modifier::BOLD),
        )),
        Spans::from(vec![
            Span::styled(
                "Current Send Rate: ",
                Style::default().add_modifier(Modifier::BOLD),
            ),
            Span::styled(
                format!(
                    "{}",
                    (app.rate_data.last().unwrap_or(&(0., 0.)).1 as usize)
                        .to_formatted_string(&Locale::en)
                ),
                Style::default()
                    .add_modifier(Modifier::BOLD)
                    .fg(Color::Green),
            ),
        ]),
        Spans::from(vec![
            Span::styled(
                "Current Error Rate: ",
                Style::default().add_modifier(Modifier::BOLD),
            ),
            Span::styled(
                format!(
                    "{}",
                    (app.error_data.last().unwrap_or(&(0., 0.)).1 as usize)
                        .to_formatted_string(&Locale::en)
                ),
                Style::default().add_modifier(Modifier::BOLD).fg(Color::Red),
            ),
        ]),
    ];

    if test_over {
        let prompt_text = format!(
            "Would you like to save this run to \"{}\" ",
            app.output_file.to_string_lossy()
        );
        f.set_cursor(
            chunks[1].x + prompt_text.len() as u16 + app.input.len() as u16 + 7,
            chunks[1].y + 4,
        );
        let prompt = Spans::from(vec![
            Span::styled(prompt_text, Style::default()),
            Span::styled("[Y/n] ", Style::default().add_modifier(Modifier::BOLD)),
            Span::styled(app.input.as_str(), Style::default()),
        ]);

        if app.input_mode == InputMode::InputEntered {
            match app.input.as_str() {
                "y" => {
                    let _ = export_csv(
                        app.rate_data.clone(),
                        app.error_data.clone(),
                        app.target_data.clone(),
                        app.packets_sent,
                        &app.output_file,
                    );
                    app.input_mode = InputMode::Quiting;
                }
                "n" => app.input_mode = InputMode::Quiting,
                _ => {
                    app.input_mode = InputMode::Editing;
                }
            }
        }
        text.push(prompt);
    }

    let p = Paragraph::new(text)
        .block(
            Block::default()
                .title(Span::styled(
                    "Information",
                    Style::default()
                        .fg(Color::Cyan)
                        .add_modifier(Modifier::BOLD),
                ))
                .title_alignment(Alignment::Center)
                .border_type(BorderType::Rounded)
                .borders(Borders::ALL),
        )
        .alignment(Alignment::Left);
    f.render_widget(p, chunks[1]);
}
