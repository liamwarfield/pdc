use pdc_core::{generator::TrafficGenerator, scenario::Scenario};
use std::{
    error::Error,
    path::PathBuf,
    thread,
    time::{Duration, Instant},
};

use crate::export_csv;

pub fn run_headless<T: 'static + Scenario>(
    mut generator: Box<dyn TrafficGenerator<T>>,
    fire: bool,
    output_file: PathBuf,
) -> Result<(), Box<dyn Error>> {
    let succ_rate_channel = generator.get_data_rate_channel();
    let error_rate_channel = generator.get_error_rate_channel();
    let tot_packet_channel = generator.get_sent_packets_channel();
    let scene = generator.get_scenario();
    let target_data = crate::create_target_dataset(scene);

    let test_duration = generator.get_scenario().duration();

    thread::spawn(move || {
        if fire {
            generator.fire_hose();
        } else {
            generator.run_scenario();
        }
    });

    let mut succ_data = Vec::new();
    let mut error_data = Vec::new();

    let mut tot_packets = 0;
    let mut error_rate = 0.;
    let mut succ_rate = 0.;

    let start = Instant::now();
    loop {
        thread::sleep(Duration::from_millis(1000));
        for rate in succ_rate_channel.try_iter() {
            succ_data.push((rate.0 as f64, rate.1 as f64));
        }

        for rate in error_rate_channel.try_iter() {
            error_data.push((rate.0 as f64, rate.1 as f64));
        }

        if let Ok(np) = tot_packet_channel.try_recv() {
            tot_packets = np;
        }

        if let Some(s) = succ_data.last() {
            succ_rate = s.1;
        }

        if let Some(e) = error_data.last() {
            error_rate = e.1;
        }

        println!(
            "Success Rate: {} Error Rate: {} Total Requests: {}",
            succ_rate, error_rate, tot_packets
        );

        if start.elapsed().as_millis() >= test_duration {
            println!(
                "Writing test results to \"{}\"",
                output_file.to_string_lossy()
            );
            let _ = export_csv(
                succ_data,
                error_data,
                target_data,
                tot_packets,
                &output_file,
            );
            return Ok(());
        }
    }
}
