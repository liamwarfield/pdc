use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct PdcArgs {
    #[structopt(subcommand)]
    pub subcommand: PdcSubCommand,
}

/// A cli for loadtesting networks and servers
#[derive(Debug, StructOpt)]
pub enum PdcSubCommand {
    /// Send one request per second to target
    Pew { url: String },
    /// Run a pdc config yaml
    Run {
        /// Path to a pdc config yaml
        config: PathBuf,
        /// Run the generator in the config at full throttle
        #[structopt(short, long)]
        fire: bool,
        /// Run pdc without the fancy tui
        #[structopt(long)]
        headless: bool,
        /// Save a CSV with this run's data
        #[structopt(short = "o", long = "output", default_value = "test_results.csv")]
        output_file: PathBuf,
    },
}
